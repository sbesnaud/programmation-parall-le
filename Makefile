-include Makefile.rules
LOGIN=PrPa_artzet-besnau-idoux
TARNAME=PrPa_artzet-besnau-idoux.tar.bz2
PROJECT=PrPa
EXEC=eratosthenesSieve

all: src

src::
		make -C src

check:: all
		make -C check

dist: distclean
		cd ..; tar cvjf ${TARNAME} ${LOGIN}

#clang:
#		make -C src clang

clean:
		make -C src clean
		make -C check clean
		rm -rf doc/html
		rm -rf doc/latex
		rm -rf *.dot

distclean: clean
		rm -f ${EXEC}
		rm -f Makefile.rules
		rm -rf doc/html doc/latex

distcheck: dist
		[ -e tmp ] || mkdir tmp
		[ -e tmp/${PROJECT} ] || mkdir tmp/${PROJECT}
		cp ../${TARNAME} tmp/${PROJET}/
		cd tmp/${PROJECT} && tar xvjf ${TARNAME}
		cd tmp/${PROJECT}/${LOGIN} && ./configure && make check

doc:: distclean
		doxygen doc/doxy.conf
		mv html latex doc

.PHONY: all check
