#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;
use Term::ANSIColor;

my ($max, $print, $version, $showoutput) = ();
GetOptions(
    'max|m=i' => \$max, #Numeric
    'print|p' => \$print, #Flag
    'version|v=s' => \$version, #Chaine
    'showoutput|o' => \$showoutput, #Flag
);


if (!defined $max)
{
    print "Veuillez fournir un nombre max\n";
    exit -1;
}

print colored ['red'], "On cherche les nombres premiers pour les $max premiers entiers\n";

if (defined $print)
{
    $print = "-l";
    print colored ['red'], "**************\n";
    print colored ['red'], "*Mode Verbeux*\n";
    print colored ['red'], "**************\n";
}
else
{
    $print = "";
}

if (!defined $version)
{
    print colored ['green'], "********************************\n";
    print colored ['green'], "*Tests avec la version normale:*\n";
    print colored ['green'], "********************************\n";
    $version = "";
}

if ($version =~ "dummy")
{
    print colored ['green'], "*****************************\n";
    print colored ['green'], "Tests avec la version dummy:*\n";
    print colored ['green'], "*****************************\n";
    $version = "-d";
}

if ($version =~ "upgrade")
{
    print colored ['green'], "*******************************\n";
    print colored ['green'], "Tests avec la version upgrade:*\n";
    print colored ['green'], "*******************************\n";
    $version = "-u";
}

my $cmd1 = ("cd ..;" .  "\\time -f 'Temps avec 1 thread: [%E]' " .
	    "./eratosthenesSieve $max 1 $version $print");

my $cmd2 = ("cd ..;" .  "\\time -f 'Temps avec 2 threads: [%E]' " .
	    "./eratosthenesSieve $max 2 $version $print");

my $cmd3 = ("cd ..;" .  "\\time -f 'Temps avec 3 threads: [%E]' " .
	    "./eratosthenesSieve $max 3 $version $print");

my $cmd4 = ("cd ..;" .  "\\time -f 'Temps avec 4 threads: [%E]' " .
	    "./eratosthenesSieve $max 4 $version $print");

if ($print !~ "-l")
{
    system($cmd1);
    system($cmd2);
    system($cmd3);
    system($cmd4);
}
else
{
    system($cmd1 . "> check/output; ./eratosthenesSieve $max -l > check/ref");
    print colored ['green'], "diff avec la ref:\n";
    my $retour = system("diff output ref");

    if (($retour >> 8) == 0)
    {
	print colored ['green'], "OK\n";
    }
    else
    {
	print colored ['red'], "KO\n";
    }

    print colored ['green'], "-------------------------------\n";

    system($cmd2 . "> check/output; ./eratosthenesSieve $max -l > check/ref");
    print colored ['green'], "diff avec la ref:\n";
    system("diff output ref");

    if (($retour >> 8) == 0)
    {
	print colored ['green'], "OK\n";
    }
    else
    {
	print colored ['red'], "KO\n";
    }

    print colored ['green'], "-------------------------------\n";

    system($cmd3 . "> check/output; ./eratosthenesSieve $max -l > check/ref");
    print colored ['green'], "diff avec la ref:\n";
    system("diff output ref");

    if (($retour >> 8) == 0)
    {
	print colored ['green'], "OK\n";
    }
    else
    {
	print colored ['red'], "KO\n";
    }

    print colored ['green'], "-------------------------------\n";

    system($cmd4 . "> check/output; ./eratosthenesSieve $max -l > check/ref");
    print colored ['green'], "diff avec la ref:\n";
    system("diff output ref");

    if (($retour >> 8) == 0)
    {
	print colored ['green'], "OK\n";
    }
    else
    {
	print colored ['red'], "KO\n";
    }

    print colored ['green'], "-------------------------------\n";

    if (defined $showoutput)
    {
	print colored ['red'], "Les nombres premiers sont:\n";
	system("cat output");
    }
}
