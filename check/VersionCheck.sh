#!/bin/bash

echo -e "\033[0;41m Tests des différents algos \033[m"
echo -e "\033[0;33m Version normale (4threads) : 10^7 nombres \033[m"
time ../eratosthenesSieve 10000000 4 -l > VersionNormale10p7.txt
echo -e "\033[0;33m Version dummies (4threads) : 10^7 nombres \033[m"
time ../eratosthenesSieve 10000000 4 -d -l > VersionDummies10p7.txt
echo -e "\033[0;33m Version upgrade (4threads) : 10^7 nombres \033[m"
time ../eratosthenesSieve 10000000 4 -u -l > VersionUpgrade10p7.txt

diff Ref10p7.txt VersionDummies10p7.txt
if [ $? -eq 0 ]
then
	echo -e "\033[1;32m DIFF : La sortie normale est pareille que la ref.\033[m"
fi
diff Ref10p7.txt VersionDummies10p7.txt
if [ $? -eq 0 ]
then
	echo -e "\033[1;32m DIFF : La sortie dummies est pareille que la ref.\033[m"
fi
diff Ref10p7.txt VersionUpgrade10p7.txt
if [ $? -eq 0 ]
then
	echo -e "\033[1;32m DIFF : La sortie upgrade est pareille que la ref.\033[m"
fi

echo -e "\033[0;41m Tests des différents algos \033[m"
echo -e "\033[0;33m Version normale (4threads) : 10^8 nombres \033[m"
time ../eratosthenesSieve 100000000 4 -l > VersionNormale10p8.txt
echo -e "\033[0;33m Version dummies (4threads) : 10^8 nombres \033[m"
time ../eratosthenesSieve 100000000 4 -d -l > VersionDummies10p8.txt
echo -e "\033[0;33m Version upgrade (4threads) : 10^8 nombres \033[m"
time ../eratosthenesSieve 100000000 4 -u -l > VersionUpgrade10p8.txt

if [ ! -f Ref10p8.txt ]
then
	cp VersionNormale10p8.txt Ref10p8.txt
fi
diff Ref10p8.txt VersionDummies10p8.txt
if [ $? -eq 0 ]
then
	echo -e "\033[1;32m DIFF : La sortie normale est pareille que la ref.\033[m"
fi
diff Ref10p8.txt VersionDummies10p8.txt
if [ $? -eq 0 ]
then
	echo -e "\033[1;32m DIFF : La sortie dummies est pareille que la ref.\033[m"
fi
diff Ref10p8.txt VersionUpgrade10p8.txt
if [ $? -eq 0 ]
then
	echo -e "\033[1;32m DIFF : La sortie upgrade est pareille que la ref.\033[m"
fi

head -664580 VersionNormale10p8.txt > 10p7primesof10p8.txt
diff VersionNormale10p7.txt 10p7primesof10p8.txt
if [ $? -eq 0 ]
then
	echo -e "\033[1;32m DIFF : Les 664580 premiers nombres premiers trouvés avec 10^7 correspondent à ceux trouvés avec 10^8.\033[m"
fi