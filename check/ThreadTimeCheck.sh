#!/bin/bash

echo -e "\033[0;41m Tests pour 10^7 nombres \033[m"
echo -e "\033[0;33m 1 thread \033[m"
time ../eratosthenesSieve 10000000 1
echo -e "\033[0;33m 2 threads \033[m"
time ../eratosthenesSieve 10000000 2
echo -e "\033[0;33m 3 threads \033[m"
time ../eratosthenesSieve 10000000 3
echo -e "\033[0;33m 4 threads \033[m"
time ../eratosthenesSieve 10000000 4
echo -e "\033[0;33m 5 threads \033[m"
time ../eratosthenesSieve 10000000 5
echo -e "\033[0;33m 6 threads \033[m"
time ../eratosthenesSieve 10000000 6
echo -e "\033[0;33m 7 threads \033[m"
time ../eratosthenesSieve 10000000 7
echo -e "\033[0;33m 8 threads \033[m"
time ../eratosthenesSieve 10000000 8

echo -e "\033[0;41m Tests pour 10^8 nombres \033[m"
echo -e "\033[0;33m 1 thread \033[m"
time ../eratosthenesSieve 100000000 1
echo -e "\033[0;33m 2 threads \033[m"
time ../eratosthenesSieve 100000000 2
echo -e "\033[0;33m 3 threads \033[m"
time ../eratosthenesSieve 100000000 3
echo -e "\033[0;33m 4 threads \033[m"
time ../eratosthenesSieve 100000000 4
echo -e "\033[0;33m 5 threads \033[m"
time ../eratosthenesSieve 100000000 5
echo -e "\033[0;33m 6 threads \033[m"
time ../eratosthenesSieve 100000000 6
echo -e "\033[0;33m 7 threads \033[m"
time ../eratosthenesSieve 100000000 7
echo -e "\033[0;33m 8 threads \033[m"
time ../eratosthenesSieve 100000000 8

echo -e "\033[0;41m Tests pour 5*10^8 nombres \033[m"
echo -e "\033[0;33m 1 thread \033[m"
time ../eratosthenesSieve 500000000 1
echo -e "\033[0;33m 2 threads \033[m"
time ../eratosthenesSieve 500000000 2
echo -e "\033[0;33m 3 threads \033[m"
time ../eratosthenesSieve 500000000 3
echo -e "\033[0;33m 4 threads \033[m"
time ../eratosthenesSieve 500000000 4
echo -e "\033[0;33m 5 threads \033[m"
time ../eratosthenesSieve 500000000 5
echo -e "\033[0;33m 6 threads \033[m"
time ../eratosthenesSieve 500000000 6
echo -e "\033[0;33m 7 threads \033[m"
time ../eratosthenesSieve 500000000 7
echo -e "\033[0;33m 8 threads \033[m"
time ../eratosthenesSieve 500000000 8
