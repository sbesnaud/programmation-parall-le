#ifndef ERATOSTHENES_HH
# define ERATOSTHENES_HH

# include <vector>
# include <iostream>
# include <cstdio>
# include <string.h>
# include <time.h>
# include <sys/time.h>
# include <stdlib.h>
# include <thread>
# include <sstream>
# include <string>
# include <time.h>

class Eratosthenes
{
  public:

    std::vector<int> primeList_;
    std::vector<int> primeListMaster_;

    Eratosthenes(int max, int nbth);

    void calc_primes(int id);
    void calc_primesDummies(int numTh);
    void calc_primesUpgrade(int id,
                            std::vector<int> primeListMaster,
                            int blockMaster,
                            int block);

    void calc_primes_master();
    void calc_primes_masterUpgrade(int max,
                                   int nbth,
                                   std::vector<int>& primeListMaster,
                                   int maxRecursive);

    void run(int print);
    void runUpgrade(int print);
    void runDummies(int print);

  private:
    // le nombre de threads utilises.
    int nbth_;
    int max_;
    // la taille d'un tableau gere par un thread = max / nbthread.
    int block_;
    // la taille d'un tableau gere par un le thread master,
    // different de la taille d'un block dans le cas d'une division non entiere
    int blockMaster_;
};

#endif /* !ERATOSTHENES_HH */
