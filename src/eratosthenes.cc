#include "eratosthenes.hh"

Eratosthenes::Eratosthenes(int max, int nbth)
{
  primeList_ = std::vector<int>(max + 1);
  primeListMaster_ = std::vector<int>(0);
  if (nbth == 1)
    nbth = 0;
  nbth_ = nbth;
  max_ = max + 1;
  block_ = (max) / (nbth + 1);
  blockMaster_ = (max) / (nbth + 1) + max % (nbth + 1);

  // On remplit la liste des max entiers.
  for (int i = 0; i <= max; ++i)
  {
    primeList_[i] = i;
  }
}

void Eratosthenes::calc_primesDummies(int numTh)
{
  int k = 2;

  for(int i = 2; i < max_; ++i)
  {
    if (i % nbth_ == numTh && primeList_[i] != 0)
    {
      for (; i * k < max_; k++)
      {
         primeList_[i * k] = 0;
      }
    }
    k = 2;
  }
}

void Eratosthenes::runDummies(int print)
{
  int i = 0;

  if (nbth_ == 0)
    nbth_ = 1;
  // Un petit tableau de threads pour faire classe.
  std::thread **workers = new std::thread*[nbth_];
  for (auto cur = workers; cur != workers + nbth_; ++cur)
  {
    // On lance un nouveau thread.
    *cur = new std::thread(&Eratosthenes::calc_primesDummies, this, i);
    i++;
  }

  for (auto cur = workers; cur != workers + nbth_; ++cur)
  {
    // On join le thread.
    (*cur)->join();
  }
  if (print)
  {
    for(unsigned int i = 0; i < primeList_.size(); ++i)
    {
      // On affiche tous les nombres premiers
      if(primeList_[i] != 0)
      {
        printf("%d\n", i);
      }
    }
  }
}


void Eratosthenes::runUpgrade(int print)
{
  std::vector<int> primeListMaster = std::vector<int>(0);

  calc_primes_masterUpgrade(max_ - 1, nbth_, primeListMaster, 0);

  if (print)
  {
    for(unsigned int i = 0; i < primeList_.size(); ++i)
    {
      // On affiche tous les nombres premiers
      if(primeList_[i] != 0)
      {
	printf("%d\n", i);
      }
    }
  }
}

void Eratosthenes::calc_primes_masterUpgrade(int max, int nbth,
                                             std::vector<int>& primeListMaster,
                                             int maxRecursive)
{
  int k = 2;
  int block = (max) / (nbth + 1);
  int blockMaster = (max) / (nbth + 1) + max % (nbth + 1);

  if (blockMaster > nbth + 1 && maxRecursive < 100)
  {
    calc_primes_masterUpgrade(blockMaster,
                              nbth,
                              primeListMaster,
                              ++maxRecursive);
  }
  else
  {
    for(int i = 2; i <= blockMaster; ++i)
    {
      if (primeList_[i])
      {
        for (; i * k <= blockMaster; ++k)
          primeList_[i * k] = 0;
        primeListMaster.push_back(i);
      }
      k = 2;
    }
  }

  if (nbth_ == 0)
    return;

  int i = 1;
  std::thread **workers = new std::thread*[nbth_];

  for (auto cur = workers; cur != workers + nbth_; ++cur)
    *cur = new std::thread(&Eratosthenes::calc_primesUpgrade,
                           this,
                           i++,
                           primeListMaster,
                           blockMaster,
                           block);

  for (auto cur = workers; cur != workers + nbth_; ++cur)
  (*cur)->join();

  for (int i = blockMaster; i < max; i++)
  {
    if (primeList_[i] != 0)
    {
      primeListMaster.push_back(i);
    }
  }
}

void Eratosthenes::calc_primesUpgrade(int id,
                                      std::vector<int> primeListMaster,
                                      int blockMaster,
                                      int block)
{
  int k = 2;

  for(int i : primeListMaster)
  {
    // On ne s'interesse qu'a un segment bien particulier de la liste des
    // entiers pour chaque thread, ici on teste la borne superieure.
    for (; i * k <= blockMaster + block * (id); ++k)
    {
      // Et ici la borne inferieure
      // (si on est en dessous aucun traitement a faire).
      if (i * k < blockMaster + block * (id - 1))
	continue;
      // On enleve de la liste des entiers les nombres non 1ers.
      primeList_[i * k] = 0;
    }
    k = 2;
  }
}

void Eratosthenes::run(int print)
{
  calc_primes_master();

  if (print)
  {
    for(unsigned int i = 0; i < primeList_.size(); ++i)
    {
      // On affiche tous les nombres premiers
      if(primeList_[i] != 0)
      {
	printf("%d\n", i);
      }
    }
  }
}

// On parcourt une 1ere fois pour etablir les nombres premiers du thread Master
void Eratosthenes::calc_primes_master()
{
  int k = 2;

  // On parcourt les (max / nbthreads) premiers entiers.
  for(int i = 2; i <= blockMaster_; ++i)
  {
    if (primeList_[i])
    {
      for (; i * k <= blockMaster_; ++k)
	primeList_[i * k] = 0;
      // On ajoute l'entier a la liste des nombres premiers du thread Master.
      primeListMaster_.push_back(i);
    }
    k = 2;
  }

  if (nbth_ == 0)
    return;

  int i = 1;
  // Un petit tableau de threads pour faire classe.
  std::thread **workers = new std::thread*[nbth_];
  for (auto cur = workers; cur != workers + nbth_; ++cur)
  {
    // On lance un nouveau thread.
    *cur = new std::thread(&Eratosthenes::calc_primes, this, i);
    i++;
  }

  for (auto cur = workers; cur != workers + nbth_; ++cur)
  {
    // On join le thread.
    (*cur)->join();
  }
}

void Eratosthenes::calc_primes(int id)
{
  int k = 2;

  // On parcourt la liste de nombres premiers etablie par le thread master
  for(int i : primeListMaster_)
  {
    // On ne s'interesse qu'a un segment bien particulier de la liste des
    // entiers pour chaque thread, ici on teste la borne superieure.
    for (; i * k <= blockMaster_ + block_ * (id); ++k)
    {
      // Et ici la borne inferieure
      // (si on est en dessous aucun traitement a faire).
      if (i * k < blockMaster_ + block_ * (id - 1))
	continue;
      // On enleve de la liste des entiers les nombres non 1ers.
      primeList_[i * k] = 0;
    }
    k = 2;
  }
}

int main(int ac, char** av)
{
  // Valeurs par defaut du nombre de threads et de la valeurs max d'entiers.
  int nbth = 1;
  int max = 100;
  int print = 0;

  enum TypeAlgo {normal, dummies, upgrade};
  TypeAlgo t = TypeAlgo::normal;

  for (int i = 1; i < ac; ++i)
  {
    if (strcmp("--help", av[i]) == 0)
    {
      printf("Liste of arguments :\n"
             "\t1) Number max\n"
             "\t2) Number of threads used\n"
             "\t3) -l to print the result\n"
             "\t4) -d to use the dummies version\n"
             "\t5) -u to use the upgrade version \n");
      return 0;
    }
    if (strcmp("-l", av[i]) == 0)
    {
      print = 1;
    }
    if (strcmp("-d", av[i]) == 0)
    {
      t = TypeAlgo::dummies;
    }
    if (strcmp("-u", av[i]) == 0)
    {
      t = TypeAlgo::upgrade;
    }
  }

  // Declaration du max par l'utilisateur.
  if (ac > 1)
    max = atoi(av[1]);
  // Declaration du nombre de threads par l'utilisateur.
  if (ac > 2)
    nbth = atoi(av[2]);

  // Le mec s'est trompe !
  if (ac > 5)
  {
    fprintf(stderr, "Invalid number of arguments\n");
    return -1;
  }

  // Constructeur.
  Eratosthenes era = Eratosthenes(max, nbth);


  switch(t)
  {
    case TypeAlgo::dummies:
      era.runDummies(print);
      break;
    case TypeAlgo::upgrade:
      era.runUpgrade(print);
      break;
    case TypeAlgo::normal:
      era.run(print);
      break;
    default:
      era.run(print);
  }

  return 0;
}
